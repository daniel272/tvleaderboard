import React from 'react';
import Admin from './admin/Admin'
import LeaderBoard from './leaderboard/LeaderBoard'
import './App.css';

function App() {
  return (
    <div className="App">
      <LeaderBoard/>
      <Admin/>
    </div>
  );
}

export default App;
