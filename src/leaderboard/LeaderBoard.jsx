import React, { useCallback, useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import './styles.css'
import axios from 'axios'

const LeaderBoard = (props) => {

  const ax = axios.create({
    baseURL: 'http://localhost:3000/'
  })

  const itemsPerPage = 12
  const timeInterval = 10000
  const mounted = useRef(false)
  const [players, setPlayers] = useState([])

  useEffect(() => {
    if (!mounted.current) {
      ax.get('players.json').then(res => setPlayers(res.data))
    }
  }, [])
  //
  // const [enter, setEnter] = useState(false)
  // const [enterDone, setEnterDone] = useState(false)
  // const [exit, setExit] = useState(false)
  // const [exitDone, setExitDone] = useState(false)

  return (
    <div className="leaderboard-wrapper d-flex">
      <div className="players-side text-center">
        <ul className="players-list">
          {new Array(12).fill('').map((x, i) =>
            <Player player={players[i]}
            />
          )}
        </ul>
      </div>
    </div>
  )
}

export default LeaderBoard