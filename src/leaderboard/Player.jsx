import React from 'react'

const Player = (props) => {
  const {player} = props

  return (
    <li
      className={`d-flex align-center player-item ${!player.position ? 'hidden' : ''}`}
      style={props.style}>
      <div className="player-position d-flex align-center">
        <div style={{
          color: props.position > 0 && props.position <= 3 ? '#FFD700' : 'white'
        }}>{props.position}</div>
      </div>
      <div className="d-flex player-info align-center" style={{
        backgroundColor: props.position > 0 && props.position <= 3 ? '#FFD700' : '#00FF85'
      }}>
        <img src={props.avatar ? `data:image/png;base64, ${props.avatar}` : Avatar} alt=""/>
        <p className="player-name">{props.name}</p>
      </div>
      <div className="player-score">
        {props.score}
      </div>
    </li>
  )
}
export default Player